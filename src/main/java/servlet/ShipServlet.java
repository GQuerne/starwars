package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOShip;
import model.Ship;




public class ShipServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static String INSERT_OR_EDIT = "/ship.jsp";
	private static String LIST_Ship = "/listship.jsp";
	private DAOShip dao;

	//    public CrewServlet() {
	//    	super();
	//    	dao = new DAOCrew();
	//    }


	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String forward="";
		String action = request.getParameter("action");

		if (action.equalsIgnoreCase("delete")){
			int shipId = Integer.parseInt(request.getParameter("id"));
			Ship crew = new Ship();
			
			try {
				Class.forName("com.mysql.jdbc.Driver");
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			crew = dao.selectById(shipId);
			
			dao.delete(crew);
			forward = LIST_Ship;
			request.setAttribute("crews", dao.selectAll());    
		} else if (action.equalsIgnoreCase("edit")){
			forward = INSERT_OR_EDIT;
			int shipId = Integer.parseInt(request.getParameter("crewId"));
			Ship ship = new Ship();
			ship = dao.selectById(shipId);
			request.setAttribute("ship", shipId);
		} else if (action.equalsIgnoreCase("listShip")){
			forward = LIST_Ship;
			request.setAttribute("crews", dao.selectAll());
		} else {
			forward = INSERT_OR_EDIT;
		}

		RequestDispatcher view = request.getRequestDispatcher(forward);
		view.forward(request, response);
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("action");
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		DAOShip daoS = new DAOShip();
		
		Ship s = new Ship();
		s.setName(request.getParameter("name"));
//		crew.setType((Type) request.getParameter("type"));
//		s.setBounty(4000);
		
		if (action.equalsIgnoreCase("insert")) {
			daoS.insert(s);
		} else if (action.equalsIgnoreCase("update")) {
			daoS.update(s);
		}
		RequestDispatcher view = request.getRequestDispatcher(LIST_Ship);
		request.setAttribute("ships", daoS.selectAll());
		view.forward(request, response);


	}
	