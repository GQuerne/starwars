package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOCrew;
import model.Crew;




public class CrewServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static String INSERT_OR_EDIT = "/crew.jsp";
	private static String LIST_Crew = "/listcrew.jsp";
	private DAOCrew dao;

	//    public CrewServlet() {
	//    	super();
	//    	dao = new DAOCrew();
	//    }


	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String forward="";
		String action = request.getParameter("action");

		if (action.equalsIgnoreCase("delete")){
			int crewId = Integer.parseInt(request.getParameter("id"));
			Crew crew = new Crew();
			
			try {
				Class.forName("com.mysql.jdbc.Driver");
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			crew = dao.selectById(crewId);
			
			dao.delete(crew);
			forward = LIST_Crew;
			request.setAttribute("crews", dao.selectAll());    
		} else if (action.equalsIgnoreCase("edit")){
			forward = INSERT_OR_EDIT;
			int crewId = Integer.parseInt(request.getParameter("crewId"));
			Crew crew = new Crew();
			crew = dao.selectById(crewId);
			request.setAttribute("crew", crew);
		} else if (action.equalsIgnoreCase("listCrew")){
			forward = LIST_Crew;
			request.setAttribute("crews", dao.selectAll());
		} else {
			forward = INSERT_OR_EDIT;
		}

		RequestDispatcher view = request.getRequestDispatcher(forward);
		view.forward(request, response);
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("action");
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		DAOCrew daoC = new DAOCrew();
		
		Crew c = new Crew();
		c.setName(request.getParameter("name"));
//		crew.setType((Type) request.getParameter("type"));
		c.setBounty(4000);
		
		if (action.equalsIgnoreCase("insert")) {
			daoC.insert(c);
		} else if (action.equalsIgnoreCase("update")) {
			daoC.update(c);
		}
		RequestDispatcher view = request.getRequestDispatcher(LIST_Crew);
		request.setAttribute("crews", daoC.selectAll());
		view.forward(request, response);
		
	}
	
//	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		Crew crew = new Crew();
//		crew.setName(request.getParameter("name"));
//		crew.setType(request.getParameter("type"));
//		try {
//			Date dob = new SimpleDateFormat("MM/dd/yyyy").parse(request.getParameter("dob"));
//			crew.setDob(dob);
//		} catch (ParseException e) {
//			e.printStackTrace();
//		}
//		crew.setEmail(request.getParameter("email"));
//		String crewid = request.getParameter("crewid");
//		if(crewid == null || crewid.isEmpty())
//		{
//			dao.insert(crew);
//		}
//		else
//		{
//			crew.(Integer.parseInt(crewid));
//			dao.update(crew);
//		}
//		RequestDispatcher view = request.getRequestDispatcher(LIST_crew);
//		request.setAttribute("crews", dao.getAllcrews());
//		view.forward(request, response);
//
//	}
}	


