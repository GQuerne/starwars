package model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Version;

@Entity
public class Crew {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	@Version
	private int version;
	private String name;
	@Enumerated(EnumType.STRING)
	private Type type;
	private double bounty;
	@OneToOne(cascade=CascadeType.ALL)
	private Ship ship;
	@ManyToMany(mappedBy="crews", fetch=FetchType.EAGER)
	private List<Mission> missions = new ArrayList<Mission>();

	public Crew() { }
	
	public Crew(String name, Type type, double bounty) {
		super();
		this.name = name;
		this.type = type;
		this.bounty = bounty;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public Type getType() {
		return type;
	}
	public void setType(Type type) {
		this.type = type;
	}

	public double getBounty() {
		return bounty;
	}
	public void setBounty(double bounty) {
		this.bounty = bounty;
	}

	public Ship getShip() {
		return ship;
	}
	public void setShip(Ship ship) {
		this.ship = ship;
	}

	public List<Mission> getMissions() {
		return missions;
	}
	public void setMissions(List<Mission> missions) {
		this.missions = missions;
	}
	public void addMission(Mission m) {
		missions.add(m);
	}

	@Override
	public String toString() {
		return "Crew [name=" + name + ", type=" + type + ", bounty=" + bounty +"]";
	}
}