package model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Version;

@Entity
public class Ship {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	@Version
	private int version;
	private String name;
	private String shipClass;
	private String shipImg;
	@OneToOne(mappedBy="ship")
	private Crew crew;

	public Ship() { }
	
	public Ship(String name, String shipClass, String shipImg) {
		this.name = name;
		this.shipClass = shipClass;
		this.shipImg = shipImg;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getShipClass() {
		return shipClass;
	}
	public void setShipClass(String shipClass) {
		this.shipClass = shipClass;
	}

	public Crew getCrew() {
		return crew;
	}
	public void setCrew(Crew crew) {
		this.crew = crew;
	}

	public String getShipImg() {
		return shipImg;
	}
	public void setShipImg(String shipImg) {
		this.shipImg = shipImg;
	}

	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}

	@Override
	public String toString() {
		return "Ship [name=" + name + ", shipClass=" + shipClass + ", shipImg=" + shipImg + "]";
	}

}
