package model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Version;

@Entity
public class Planet {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	@Version
	private int version;	
	private String name;
	private String sector;
	private String lang;
	private String planetImg;
	@OneToMany(mappedBy="quest", fetch=FetchType.EAGER)
	private List<Mission> quests;
	@OneToMany(mappedBy="objective", fetch=FetchType.EAGER)
	private List<Mission> objectives;
	
	public Planet() { }
	
	public Planet(String name, String sector, String lang, String planetImg) {
		this.name = name;
		this.sector = sector;
		this.lang = lang;
		this.planetImg = planetImg;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getSector() {
		return sector;
	}
	public void setSector(String sector) {
		this.sector = sector;
	}

	public String getLang() {
		return lang;
	}
	public void setLang(String lang) {
		this.lang = lang;
	}

	public String getPlanetImg() {
		return planetImg;
	}
	public void setPlanetImg(String planetImg) {
		this.planetImg = planetImg;
	}

	public List<Mission> getQuests() {
		return quests;
	}
	public void setQuests(List<Mission> quests) {
		this.quests = quests;
	}
	public void addQuests(Mission m) {
		quests.add(m);
	}

	public List<Mission> getObjectives() {
		return objectives;
	}
	public void setObjectives(List<Mission> objectives) {
		this.objectives = objectives;
	}
	public void addObjectives(Mission m) {
		objectives.add(m);
	}

	@Override
	public String toString() {
		return "Planet [name=" + name + ", sector=" + sector + ", lang=" + lang + ", planetImg=" + planetImg + "]";
	}
	
}
