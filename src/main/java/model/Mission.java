package model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Version;

@Entity
public class Mission {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	@Version
	private int version;
	private String name;
	private int level;
	private int reward;
	private String status;
	@ManyToOne
	private Planet quest;
	@ManyToOne
	private Planet objective;
	@ManyToMany(fetch=FetchType.EAGER)
	private List<Crew> crews;
	
	public Mission() {	}

	public Mission(String name, int level, int reward, String status) {
		this.name = name;
		this.level = level;
		this.reward = reward;
		this.status = status;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}

	public int getReward() {
		return reward;
	}
	public void setReward(int reward) {
		this.reward = reward;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Planet getQuest() {
		return quest;
	}
	public void setQuest(Planet quest) {
		this.quest = quest;
	}

	public Planet getObjective() {
		return objective;
	}
	public void setObjective(Planet objective) {
		this.objective = objective;
	}

	public List<Crew> getCrews() {
		return crews;
	}
	public void setCrews(List<Crew> crews) {
		this.crews = crews;
	}
	public void addCrew(Crew c) {
		crews.add(c);
	}

	@Override
	public String toString() {
		return "Mission [name=" + name + ", level=" + level + ", reward=" + reward + ", status=" + status +"]";
	}

}
