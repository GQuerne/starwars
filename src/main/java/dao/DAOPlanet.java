package dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import model.Planet;
import util.Context;

public class DAOPlanet {

	public void insert(Planet p) {
		EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();

		em.getTransaction().begin();
		em.persist(p);

		em.getTransaction().commit();
		em.close(); Context.destroy();
	}

	public void update(Planet p) {
		EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();

		em.getTransaction().begin();
		em.merge(p);

		em.getTransaction().commit();
		em.close(); Context.destroy();
	}

	public Planet selectById(int id) {
		EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();

		Planet p = em.find(Planet.class, id);
		System.out.println(p);

		em.close(); Context.destroy();
		return p;
	}

	public List<Planet> selectAll() {
		EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();

		Query query = em.createQuery("Select a from Planet p");
		List<Planet> planet = query.getResultList();

		em.close(); Context.destroy();
		return planet;
	}

	public void delete(Planet p) {
		EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();

		em.getTransaction().begin();
		em.remove(em.merge(p));

		em.getTransaction().commit();
		em.close(); Context.destroy();
	}

}