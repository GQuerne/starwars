package dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import model.Mission;
import util.Context;

public class DAOMission {

	public void insert(Mission m) {
		EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();

		em.getTransaction().begin();
		em.persist(m);

		em.getTransaction().commit();
		em.close();	Context.destroy();		
	}

	public void update(Mission m) {
		EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();

		em.getTransaction().begin();
		em.merge(m);

		em.getTransaction().commit();
		em.close(); Context.destroy();
	}

	public Mission selectById(int id) {
		EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();

		Mission m = em.find(Mission.class, id);
		System.out.println(m);

		em.close(); Context.destroy();
		return m;
	}

	public List<Mission> selectAll() {
		EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();

		Query query = em.createQuery("Select a from Mission m");
		List<Mission> missions = query.getResultList();

		em.close();
		Context.destroy();
		return missions;
	}

	public void delete(Mission m) {
		EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();

		em.getTransaction().begin();
		em.remove(em.merge(m));
		
		em.getTransaction().commit();
		em.close(); Context.destroy();
	}

}
