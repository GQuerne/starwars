package dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import model.*;
import util.Context;

public class DAOShip {

	public void insert(Ship s) {
		EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();

		em.getTransaction().begin();
		em.persist(s);

		em.getTransaction().commit();
		em.close(); Context.destroy();

	}

	public void update(Ship s) {
		EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();

		em.getTransaction().begin();
		em.merge(s);

		em.getTransaction().commit();
		em.close(); Context.destroy();
	}

	public Ship selectById(int id) {
		EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();

		Ship s = em.find(Ship.class, id);
		System.out.println(s);

		em.close();
		Context.destroy();
		return s;

	}

	public List<Ship> selectAll() {
		EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();

		Query query = em.createQuery("Select a from Ship s");
		List<Ship> ship = query.getResultList();

		em.close(); Context.destroy();
		return ship;
	}

	public void delete(Ship s) {
		EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();

		em.getTransaction().begin();
		em.remove(em.merge(s));

		em.getTransaction().commit();
		em.close(); Context.destroy();

	}

}
