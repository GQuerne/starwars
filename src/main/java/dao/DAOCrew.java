package dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import model.Crew;
import util.Context;

public class DAOCrew {

	public void insert(Crew c) {
		EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();

		em.getTransaction().begin();
		em.persist(c);

		em.getTransaction().commit();
		em.close(); Context.destroy();
	}

	public void update(Crew c) {
		EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();

		em.getTransaction().begin();
		em.merge(c);

		em.getTransaction().commit();
		em.close(); Context.destroy();
	}

	public Crew selectById(int id) {
		EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();

		Crew c = em.find(Crew.class, id);
		System.out.println(c);

		em.close(); Context.destroy();
		return c;
	}

	public List<Crew> selectAll() {
		EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();

		Query query = em.createQuery("Select a from Crew c");
		List<Crew> crews = query.getResultList();

		em.close(); Context.destroy();
		return crews;
	}

	public void delete(Crew c) {
		EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();

		em.getTransaction().begin();
		em.remove(em.merge(c));

		em.getTransaction().commit();
		em.close(); Context.destroy();
	}
}