package Sw.StarWars;

import javax.persistence.EntityManager;

import model.Crew;
import model.Mission;
import model.Planet;
import model.Ship;
import model.Type;
import util.Context;

public class App 
{
	public static void main( String[] args )
	{
		generateDB();
	}

	public static void generateDB() {
		EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();
		em.getTransaction().begin();

		Mission m1 = new Mission("Assassinate", 5, 15000000, "available");
		
		Planet p1 = new Planet("Coruscant", "Corusca sector", "Galactic Basic", "https://vignette.wikia.nocookie.net/starwars/images/8/84/CoruscantGlobeE1.png/revision/latest/scale-to-width-down/500?cb=20130123002137");
		Planet p2 = new Planet("Naboo", "Chommell sector", "Galactic Basic", "https://vignette.wikia.nocookie.net/starwars/images/5/50/Naboo.jpg/revision/latest/scale-to-width-down/350?cb=20080723001712");
		
		Crew c1 = new Crew("Maul", Type.Sith, 0);
		
		Ship s1 = new Ship("Scimitar", "Starfighter", "https://vignette.wikia.nocookie.net/starwars/images/2/2e/Scimitar_BF2.png/revision/latest/scale-to-width-down/500?cb=20170825000614");
		
		em.persist(m1);
		em.persist(p1);
		em.persist(p2);
		em.persist(c1);
		em.persist(s1);

		em.getTransaction().commit();
		Context.destroy();
	}

}
